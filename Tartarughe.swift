//
//  Tartarughe.swift
//  SpaceMouse
//
//  Created by Olga Rastelli on 27/02/18.
//  Copyright © 2018 Olga Rastelli. All rights reserved.
//


import SpriteKit

class Tartarughe: SKSpriteNode {
    
    init() {
        let texture = SKTexture(imageNamed: "turtleorange")
        super.init(texture: texture, color: .clear, size: texture.size())
        let physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        physicsBody.contactTestBitMask = 1
        self.physicsBody = physicsBody
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

 