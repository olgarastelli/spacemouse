//
//  GameScene.swift
//  SpaceMouse
//
//  Created by Olga Rastelli on 27/02/18.
//  Copyright © 2018 Olga Rastelli. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    private var gameRunning = true//Indica se il gioco è in esecuzione (true) o se è terminato (false)
    private var mouse: Mouse! //Riferimento allo sprite del protagonista
    private var didGetCoin = false//Viene impostata a true ogni volta che rileviamo una collisione tra un oggetto Coin e Mouse
    private var collectedCoins = 0//Rappresenta il numero di Coin raccolti
    private var scoreLabel: SKLabelNode!//Riferimento alla label aggiunta poc’anzi con l’editor grafico
    
    override func didMove(to view: SKView) {
        startGame()
    }
    
    
    private func startGame() {
        mouse = Mouse(gameScene: self)
        mouse.position = CGPoint(x: -frame.width / 4, y: frame.midY)
        addChild(mouse)
        addElementsForever()//che si occuperà di aggiungere in continuazione sprite di tipo Coin o Block, e di animarli.
        self.physicsWorld.contactDelegate = self //Stiamo anche indicando che questa scena è il contactDelegate del mondo fisico simulato. In questo modo, riceveremo notifiche relative alle collisioni che avvengono
        self.physicsWorld.gravity = .zero
        scoreLabel = childNode(withName: "scoreLabel") as! SKLabelNode
        collectedCoins = 0
        scoreLabel.text = "0"
        gameRunning = true
    }
    
    //Altro metodo già discusso in una sua precedente versione è touchesBegan, che si limitava ad invocare il metodo fly() di Mouse. Adesso, esso controlla anche se il gioco è in esecuzione, e solo in tal caso richiama mouse.fly(); altrimenti, riavvia la partita.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if gameRunning {
            mouse.fly()
        } else {
            restart()
        }
    }
    //Il metodo restart ripulisce la scena eliminando tutti gli sprite di tipo Coin, Block e Mouse, per poi invocare startGame.
    private func restart() {
        children.filter { $0 is Coin  || $0 is Mouse || $0 is Block  }.forEach { $0.removeFromParent() }
        startGame()
    }
    //Di fondamentale importanza per tutte le app basate su SpriteKit è il metodo update, che viene eseguito automaticamente ogni 16 millisecondi e rappresenta la nostra opportunità di aggiornare e/o preparare il prossimo frame. In particolare, lo utilizziamo controllare se si è verificata una collisione tra gli sprite Mouse e Coin. In tal caso, incrementiamo il contatore dello score (collectedCoins) e aggiorniamo la label scoreLabel.
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if didGetCoin {
            didGetCoin = false
            collectedCoins += 1
            scoreLabel.text = "\(collectedCoins)"
        }
    }
    
    func didDie() {
        gameRunning = false
    }
    
    //Il primo si occupa semplicemente di invocare addElement ogni mezzo secondo.
    private func addElementsForever() {
        let addElement = SKAction.run {
            self.addElement()
        }
        let wait = SKAction.wait(forDuration: 0.5)
        let sequence = SKAction.sequence([addElement, wait])
        let forever = SKAction.repeatForever(sequence)
        self.run(forever)
    }
  
    //Il secondo metodo posiziona casualmente un Coin o un Block in una coordinata Y casuale, associandolo piu ad un’animazione che lo muoverà da sinistra verso destra.
      private func addElement()  {
        let element = arc4random_uniform(2) == 0 ? Coin() : Block();
        let randomY = CGFloat(arc4random_uniform(UInt32(self.frame.height - 100))) + 50
        element.position =  CGPoint(x: self.frame.maxX + element.frame.width, y: randomY - self.frame.height / 2 )
        self.addChild(element)
        let move = SKAction.moveTo(x: -self.frame.width, duration: 3)
        
        let sequence = SKAction.sequence([move, SKAction.removeFromParent()])
        element.run(sequence)
        
    }
    //
    func didBegin(_ contact: SKPhysicsContact) {
        
        guard let otherNode = contact.bodyA.node is Mouse ? contact.bodyB.node : contact.bodyA.node else { return }
        
        if let coin = otherNode as? Coin {
            coin.removeFromParent()
            self.didGetCoin = true
        }
        
        if otherNode is Block {
            gameRunning = false
            self.removeAllActions()
            self.children.forEach { $0.removeAllActions() }
            scoreLabel.text = " *** GAME OVER *** Score: \(collectedCoins)"
            mouse.die()
        }
    }
    
}
