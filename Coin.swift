//
//  Coin.swift
//  SpaceMouse
//
//  Created by Olga Rastelli on 27/02/18.
//  Copyright © 2018 Olga Rastelli. All rights reserved.
//

//Si tratta di una classe piuttosto semplice. Tutta la logica è nell’inizializzatore, che ha il compito di caricare l’immagine della moneta, creare la texture ed applicarla allo sprite corrente.Inoltre, viene anche definito un corpo fisico per questo sprite che useremo per rilevare la collisione.
import SpriteKit

class Coin: SKSpriteNode {
    
    init() {
        let texture = SKTexture(imageNamed: "object_coin")
        super.init(texture: texture, color: .clear, size: texture.size())
        let physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        physicsBody.contactTestBitMask = 1
        self.physicsBody = physicsBody
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
